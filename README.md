Nunjucks I18N
=============

Internationalisation helpers for Nunjucks.

Installation
------------

```bash
# With Yarn
yarn add https://bitbucket.org/getbellhop/nunjucks-i18n.git
# With NPM
npm install --save https://bitbucket.org/getbellhop/nunjucks-i18n.git
```

Then connect with your Nunjucks environment and your localising system:

```js
const nunjucksI18n = require('nunjucks-i18n');

// (Setup Nunjucks environment, eg: const env = new nunjucks.Environment(...);

const localizer = (key, params, nunjucksContext, nunjucksGlobals) => {
    // Integrate with your localising system here, eg:
    return (new IntlMessageFormat(messages[key], nunjucksContext.locale)).localize(params);
};
nunjucksI18n.connectMessages(env, localizer);
```

If your localising system supports it, you can also enable support for number, absolute date, and relative date localisers:

```js
nunjucksI18n.connectNumbers(env, (value, options, nunjucksContext, nunjucksGlobals) => myLocalizingSystem.localizeNumber(value));
nunjucksI18n.connectDates(env, (date, options, nunjucksContext, nunjucksGlobals) => myLocalizingSystem.localizeDate(date));
nunjucksI18n.connectRelativeDates(env, (date, options, nunjucksContext, nunjucksGlobals) => myLocalizingSystem.localizeRelativeDate(date));
```

_(`options` is the first argument provided to each of the three filters, if any)_

Note that the returned value from any localising function will be HTML-escaped by Nunjucks. To return raw HTML, return an instance of `nunjucks.runtime.SafeString` (e.g. `return new nunjucks.runtime.SafeString('(Your localised HTML)')`).


Use in Templates
----------------

### Messages

```html
<h1>{{ _('page-heading') }}</h1>
<p>{{ _('user-welcome', { username: user.name }) }}</p>
```

### Numbers

```html
<p>{{ user.postCount | number }}</p>
```

Custom formatting options (dependent on your localisation system) can be provided as the first argument:

```
<p>{{ account.balance | number({ currency: 'AUD' }) }}</p>
<p>{{ trip.distance | number({ unit: 'km' }) }}</p>
```

### Dates

```html
<p>{{ post.date | date }}</p>
```

Custom formatting options (dependent on your localisation system) can be provided as the first argument:

```
<p>{{ post.date | date('long') }}</p>
<p>{{ post.date | date({ format: 'YYYYMMDD' }) }}</p>
```

Relative dates follow a similar pattern:

```html
<p>{{ post.date | relativeDate }}</p>
```

### Complex Messages

For localised paragraphs containing one or more complex HTML elements, use the `{% _ %}` block. `{% fragment %}` blocks within will be inserted into the final localised message, replacing `[[fragmentName]]`.

```html
<div class="hero">
    {% _('page-hero') %}
        {% fragment 'selector' %}
            <select>
                <!-- ... -->
            </select>
        {% endfragment %}
    {% end_ %}
</div>
```

In the above example, given the localised string `Start learning [[selector]] today!`, the following HTML will be produced:

```html
<div class="hero">
    I want to learn <select><!-- ... --></select> today!
</div>
```


Extra Details
-------------

It is recommended to assign the current user’s locale to the current Nunjucks context when rendering (e.g. `env.render('template.njk', { locale: 'en-AU' })`). Then the locale can be extracted from the current context in the localiser function. Additional user details, such as preferred timezone, date format, etc can also be defined in the context. Global settings can either be declared directly in the external localiser, or as Nunjucks globals, which are also provided to the localiser function.
