import * as nunjucks from 'nunjucks';

declare namespace NunjucksI18n {
    export type MessagesLocaliser = (key: string, params: any, context: any, globals: any) => null;
    export function connectMessages(env: nunjucks.Environment, localizer: MessagesLocaliser): void;

    export type NumbersLocaliser = (value: number, options: any, context: any, globals: any) => null;
    export function connectNumbers(env: nunjucks.Environment, localizer: NumbersLocaliser): void;

    export type DatesLocaliser = (date: Date, options: any, context: any, globals: any) => null;
    export function connectDates(env: nunjucks.Environment, localizer: DatesLocaliser): void;
    export function connectRelativeDates(env: nunjucks.Environment, localizer: DatesLocaliser): void;
}
