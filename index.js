const FragmentsExtension = require('./lib/FragmentsExtension');

const NunjucksI18n = {
    connectMessages(env, localizer) {
        // Simple function
        env.addGlobal('_', function(key, params) {
            return localizer(key, params, this.ctx, this.env.globals);
        });

        // Complex fragments
        env.addExtension('I18nFragmentsExtension', new FragmentsExtension(localizer));
    },

    connectNumbers(env, localiser) {
        env.addFilter('number', function(value, options) {
            return localiser(value, options, this.ctx, this.env.globals);
        });
    },

    connectDates(env, localiser) {
        env.addFilter('date', function(date, options) {
            return localiser(date, options, this.ctx, this.env.globals);
        });
    },

    connectRelativeDates(env, localiser) {
        env.addFilter('relativeDate', function(date, options) {
            return localiser(date, options, this.ctx, this.env.globals);
        });
    },
};

module.exports = NunjucksI18n;
