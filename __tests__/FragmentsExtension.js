const nunjucks = require('nunjucks');
const nunjucksI18n = require('../index');
const FragmentsExtension = require('../lib/FragmentsExtension');

const templateName = 'tpl.njk';

function buildEnv(templateContent, strings, fragments) {
    const env = new nunjucks.Environment(
        {
            getSource() {
                return {
                    path: templateName,
                    src: templateContent,
                    noCache: true,
                };
            },
        },
        { dev: true },
    );

    nunjucksI18n.connectMessages(env, (key, params, context, globals) => {
        return strings[key];
    });

    return env;
}

function render(message, fragments = '') {
    const env = buildEnv(`{% _('test') %}${fragments}{% end_ %}`, {
        test: message,
    });

    return env.render(templateName);
}

test('no fragments', () => {
    const rendered = render('Test content');
    expect(rendered).toBe('Test content');
});

test('no fragments escaping HTML', () => {
    const rendered = render('<Test content>');
    expect(rendered).toBe('&lt;Test content&gt;');
});

test('simple fragments', () => {
    const rendered = render('Test [[fragment-one]] content [[fragment-two]]', `
{% fragment 'fragment-one' -%}
    fragment one content
{%- endfragment %}
{% fragment 'fragment-two' -%}
    fragment two content
{%- endfragment %}
`);
    expect(rendered).toBe('Test fragment one content content fragment two content');
});

test('HTML fragments', () => {
    const rendered = render('Test [[fragment-one]] content [[fragment-two]]', `
{% fragment 'fragment-one' -%}
    <strong>fragment one content</strong>
{%- endfragment %}
{% fragment 'fragment-two' -%}
    <strong>fragment two content</strong>
{%- endfragment %}
`);
    expect(rendered).toBe('Test <strong>fragment one content</strong> content <strong>fragment two content</strong>');
});

test('HTML fragments containing Nunjucks', () => {
    const rendered = render('Test [[fragment-one]] content [[fragment-two]]', `
{% fragment 'fragment-one' -%}
    <strong>fragment one {{ '< test content >' | title }} content</strong>
{%- endfragment %}
{% fragment 'fragment-two' -%}
    <strong>fragment two {% if true -%} a {%- else -%} b {%- endif %} content</strong>
{%- endfragment %}
`);
    expect(rendered).toBe('Test <strong>fragment one &lt; Test Content &gt; content</strong> content <strong>fragment two a content</strong>');
});

test('Pre-escaped message', () => {
    const rendered = render(new nunjucks.runtime.SafeString('<p>Test [[fragment]] content</p>'), `
{% fragment 'fragment' -%}
    <strong>fragment</strong>
{%- endfragment %}
`);
    expect(rendered).toBe('<p>Test <strong>fragment</strong> content</p>');
});
