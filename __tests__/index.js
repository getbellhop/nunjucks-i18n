const FragmentsExtension = require('../lib/FragmentsExtension');
const NunjucksI18n = require('../index');

const dummyOptions = { one: 1, two: 2 };
const dummyResult = 'returnValue';
const dummyNunjucks = {
    ctx: '{{context}}',
    env: {
        globals: '{{globals}}',
    },
};

function assertConnection(name, fn, expectedArgs) {
    const mockAddFilter = jest.fn(() => null);
    const mockLocaliser = jest.fn(() => dummyResult);

    fn.call(NunjucksI18n, {
        addFilter: mockAddFilter,
    }, mockLocaliser);

    // Ensure called correctly
    expect(mockAddFilter.mock.calls.length).toBe(1);
    expect(mockAddFilter.mock.calls[0][0]).toBe(name);

    const trigger = mockAddFilter.mock.calls[0][1];
    expect(typeof trigger).toBe('function');

    assertLocaliserCall(trigger, mockLocaliser.mock, ...expectedArgs);
}

function assertLocaliserCall(trigger, mock, ...expectedArgs) {
    trigger.call(dummyNunjucks, ...expectedArgs);

    expect(mock.calls.length).toBe(1);

    // Ensure expected args passed through correctly
    for (let i = 0; i < expectedArgs.length; i++) {
        expect(mock.calls[0][i]).toBe(expectedArgs[i]);
    }

    // Ensure context passed through correctly
    expect(mock.calls[0][expectedArgs.length]).toBe(dummyNunjucks.ctx);
    expect(mock.calls[0][expectedArgs.length + 1]).toBe(dummyNunjucks.env.globals);
}

test('connect messages', () => {
    const mockAddGlobal = jest.fn(() => null);
    const mockAddExtension = jest.fn(() => null);
    const mockLocaliser = jest.fn(() => dummyResult);

    NunjucksI18n.connectMessages.call(NunjucksI18n, {
        addGlobal: mockAddGlobal,
        addExtension: mockAddExtension,
    }, mockLocaliser);

    // Ensure global called correctly
    expect(mockAddGlobal.mock.calls.length).toBe(1);
    expect(mockAddGlobal.mock.calls[0][0]).toBe('_');

    const trigger = mockAddGlobal.mock.calls[0][1];
    expect(typeof trigger).toBe('function');

    assertLocaliserCall(trigger, mockLocaliser.mock, 'dummyKey', dummyOptions);

    // Ensure extension called correctly
    expect(mockAddExtension.mock.calls.length).toBe(1);
    expect(mockAddExtension.mock.calls[0][0]).toBe('I18nFragmentsExtension');
    const extension = mockAddExtension.mock.calls[0][1];
    expect(extension).toBeInstanceOf(FragmentsExtension);
    expect(extension.localizer).toBe(mockLocaliser);
});

test('connect numbers', () => {
    assertConnection('number', NunjucksI18n.connectNumbers, [123, dummyOptions]);
});

test('connect dates', () => {
    assertConnection('date', NunjucksI18n.connectDates, [new Date(), dummyOptions]);
});

test('connect relative dates', () => {
    assertConnection('relativeDate', NunjucksI18n.connectRelativeDates, [new Date(), dummyOptions]);
});
