const nunjucks = require('nunjucks');
const SafeString = nunjucks.runtime.SafeString;

module.exports = class FragmentsExtension {
    constructor(localizer) {
        this.tags      = ['_', 'fragment'];
        this.localizer = localizer;

        this.fragmentOpen  = '[[';
        this.fragmentClose = ']]';

        this.ignoreUnknownFragments = false;
    }

    parse(parser, nodes) {
        // Handle opening tag
        const token = parser.nextToken();
        if (token.value !== this.tags[0]) {
            throw 'Invalid "' + token.value + '" outside of ' + this.tags[0];
        }
        const args = parser.parseSignature(null, false);
        parser.advanceAfterBlockEnd(token.value);

        // Find fragment or end
        parser.parseUntilBlocks('end' + this.tags[0], this.tags[1]);

        // Process any fragments
        const fragmentBodies = [];
        while (parser.skipSymbol(this.tags[1])) {
            // Handle header
            const fragmentArgs = parser.parseSignature(null, true);
            parser.advanceAfterBlockEnd(this.tags[1]);

            // Include fragment args with parent args
            fragmentArgs.children.map(child => args.children.push(child));

            // Handle body
            const fragmentBody = parser.parseUntilBlocks('end' + this.tags[1]);
            parser.advanceAfterBlockEnd();

            // Include fragment bodies
            fragmentBodies.push(fragmentBody);

            // Look for end/next fragment
            parser.parseUntilBlocks('end' + this.tags[0], this.tags[1]);
        }

        // Move to end of block
        parser.advanceAfterBlockEnd();

        return new nodes.CallExtension(this, 'run', args, fragmentBodies);
    }

    run(context, key, ...fragmentsAndFragmentKeys) {
        // Extract params (if set)
        let params = {};
        if (fragmentsAndFragmentKeys[0] != null && typeof fragmentsAndFragmentKeys[0] === 'object') {
            params = fragmentsAndFragmentKeys.shift();
        }

        // Render message
        let rendered = this.localizer(key, params, context.ctx, context.env.globals);
        if (!(rendered instanceof SafeString)) {
            // Param not yet escaped - escape
            rendered = nunjucks.lib.escape(rendered);
        }

        // Apply fragments
        for (let i = 0, limit = fragmentsAndFragmentKeys.length / 2; i < limit; i++) {
            const fragmentKey  = fragmentsAndFragmentKeys[i];
            const fragmentBody = fragmentsAndFragmentKeys[i + limit]();

            let isMatched = false;
            rendered = rendered.replace(this.fragmentOpen + fragmentKey + this.fragmentClose, () => {
                isMatched = true;
                return fragmentBody;
            });
            if (!isMatched && !this.ignoreUnknownFragments) {
                throw `Unknown fragment "${fragmentKey}"`;
            }
        }

        return new SafeString(rendered);
    }
};
